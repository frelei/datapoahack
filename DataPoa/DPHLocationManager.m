//
//  DPHLocationManager.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHLocationManager.h"
#import "DPHHospital.h"
#import "DPHSetting.h"

@implementation DPHLocationManager
+ (DPHHospital *)verifyLocationNearHospital:(CLLocation *)location
{
    NSArray *hospitalList = [[DPHSetting shareInstance] getHospitalList];
    CLLocation *location2;
    for (DPHHospital *hospital in hospitalList)
    {
        location2 = [[CLLocation alloc] initWithLatitude:hospital.latitude longitude:hospital.longitude];
        CLLocationDistance distance = [location distanceFromLocation:location2];
    
        if (distance < 200)
            return hospital;
    }
    return nil;
}

@end
