//
//  DPHFollowing.m
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHFollowing.h"

@implementation DPHFollowing

@dynamic followedUser;
@dynamic followingUser;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Following";
}

@end
