//
//  DPHLinkUsersViewController.m
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHLinkUsersViewController.h"
#import <Parse/Parse.h>
#import "DPHUser.h"
#import "DPHFollowing.h"

@interface DPHLinkUsersViewController ()
@property(nonatomic)NSMutableArray* usersToBeFollowed;
@end

@implementation DPHLinkUsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#warning chamar esse método quando for resign modal
-(void)saveInParseServerTheLinkedUsers{
    
    for (int i=0; i<self.usersToBeFollowed.count; i++) {
        
        DPHFollowing* following = [DPHFollowing objectWithClassName:[DPHFollowing parseClassName]];
        following.followedUser=self.usersToBeFollowed[i];
        following.followingUser=(DPHUser*)[PFUser currentUser];
        [following save:nil];
        
        if (i==self.usersToBeFollowed.count-1) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

-(void)getUsersWithUsername:(NSString*)username{
    
    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"username" equalTo:username];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
#warning lista de objetos com este username
            
            for (PFObject *object in objects) {
                NSLog(@"%@", object.objectId);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

#warning chamar este método cada vez que o user selecionar um user como contato de emergência
-(void)didSelectUser:(DPHUser*)user{
    [self.usersToBeFollowed addObject:user];
}

-(NSMutableArray *)usersToBeFollowed{
    if (!_usersToBeFollowed) {
        _usersToBeFollowed = [[NSMutableArray alloc]init];
    }
    return _usersToBeFollowed;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
