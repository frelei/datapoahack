//
//  DPHNotificationViewController.m
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHNotificationVC.h"
#import "DPHWSDataPoa.h"
#import "DPHSetting.h"

@interface DPHNotificationVC ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DPHNotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getHospitalList];
    [self configureTableViewSettings];
}

#pragma mark DATA

- (void)getNotifications
{
    self.tableViewList = [[NSMutableArray alloc] init];
#warning PUXAR AQUI
}

#pragma mark TABLEVIEW

- (void)configureTableViewSettings
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tabBarController.tabBar.frame.size.height)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
    imageView.layer.borderWidth = 1.;
    imageView.layer.borderColor = [UIColor clearColor].CGColor;
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.clipsToBounds = YES;
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:2];
    UILabel *lblStats = (UILabel *)[cell viewWithTag:3];
    UILabel *lblTime = (UILabel *)[cell viewWithTag:4];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

#pragma mark - GETHOSPITAL

- (void)getHospitalList
{
    DPHWSDataPoa *dataPoa = [[DPHWSDataPoa alloc] init];
    [dataPoa getPlacesInformationWithCallback:^(NSArray * list) {
        if (list.count > 0)
        {
            [[DPHSetting shareInstance] storeHospitalList:list];
        }
    }];

    if ([[DPHSetting shareInstance] getTestBackgroundRefresh])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Test" message:@"Funcionou" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

@end
