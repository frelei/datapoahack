//
//  DPHPushNotification.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPHHospital.h"

@interface DPHPushNotification : NSObject
+ (void)verifyHospitalToSendPush:(DPHHospital *)hospital; 
@end
