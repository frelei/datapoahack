//
//  DPHHospital.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHHospital.h"

@implementation DPHHospital
- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.ID = [aDecoder decodeIntForKey:@"ID"];
        self.instituteType = [aDecoder decodeObjectForKey:@"instituteType"];
        self.emergencyType = [aDecoder decodeObjectForKey:@"emergencyType"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.latitude = [aDecoder decodeFloatForKey:@"latitude"];
        self.longitude = [aDecoder decodeFloatForKey:@"longitude"];
        self.linkPT = [aDecoder decodeObjectForKey:@"linkPT"];
        self.linkEN = [aDecoder decodeObjectForKey:@"linkEN"];
        self.linkES = [aDecoder decodeObjectForKey:@"linkES"];
        self.specialityPT = [aDecoder decodeObjectForKey:@"specialityPT"];
        self.specialityEN = [aDecoder decodeObjectForKey:@"specialityEN"];
        self.specialityES = [aDecoder decodeObjectForKey:@"specialityES"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInt:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.instituteType forKey:@"instituteType"];
    [aCoder encodeObject:self.emergencyType forKey:@"emergencyType"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.phone forKey:@"phone"];
    [aCoder encodeFloat:self.latitude forKey:@"latitude"];
    [aCoder encodeFloat:self.longitude forKey:@"longitude"];
    [aCoder encodeObject:self.linkPT forKey:@"linkPT"];
    [aCoder encodeObject:self.linkEN forKey:@"linkEN"];
    [aCoder encodeObject:self.linkES forKey:@"linkES"];
    [aCoder encodeObject:self.specialityPT forKey:@"specialityPT"];
    [aCoder encodeObject:self.specialityEN forKey:@"specialityEN"];
    [aCoder encodeObject:self.specialityES forKey:@"specialityES"];
}
@end
