//
//  DPHInfo.m
//  DataPoa
//
//  Created by Fabio Barboza on 12/13/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHInfo.h"

@implementation DPHInfo

- (instancetype)initWithTitle:(NSString *)title andDetail:(NSString *)detail
{
    self = [super init];
    if (self) {
        self.title = title;
        self.detail = detail;
    }
    return self;
}

@end
