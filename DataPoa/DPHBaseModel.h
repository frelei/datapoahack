//
//  ACCBaseModel.h
//  Atlanta Chamber
//
//  Created by Rodrigo Freitas Leite on 10/12/14.
//  Copyright (c) 2014 arctouch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface DPHBaseModel : NSObject
{
    NSMutableDictionary *attrs;
}

@property (strong) NSMutableDictionary *attrs;

- (id)initWithAttrs:(NSMutableDictionary *) attributes;

- (NSInteger) ID;

- (NSString *) returnNSString:(id)object;
- (NSInteger) returnNSInteger:(id)object;
- (CGFloat) returnCGFloat:(id)object;
- (BOOL) returnBOOL:(id)object;
- (NSDate *) returnNSDate:(id)object withFormat:(NSString *)format;
- (NSDate *) returnNSDate:(id)object;
- (NSDate *) returnNSDateFromTimeInterval:(id)object;
- (NSDate *)returnNSDateFromDate:(id)object;
- (NSData *) returnNSData:(id)object;
- (NSDictionary *) returnNSDictionary:(id)object;
- (NSArray *) returnNSArray:(id)object;
- (long)returnLong:(id)object;
- (NSArray *) returnNSArray:(id)object withObjectType:(Class) type;
- (DPHBaseModel *) returnObject:(id)object withType:(Class) type;


- (void)encodeWithCoder:(NSCoder *)coder;
- (id) initWithCoder: (NSCoder *)coder;
@end
