//
//  DPHLoginViewController.m
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHLoginViewController.h"
#import "DPHSetting.h"
#import "DPHUser.h"

@interface DPHLoginViewController ()

@end

@implementation DPHLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtPassword.delegate = self;
    self.txtUsername.delegate = self;
}

- (IBAction)singupAction:(UIButton *)sender {
    if (self.txtUsername.text.length > 0 && self.txtPassword.text > 0)
        [self registerNewUserWithUsername:self.txtUsername.text andPassword:self.txtPassword.text];
}

- (IBAction)loginAction:(UIButton *)sender {
    
    if (self.txtUsername.text.length > 0 && self.txtPassword.text > 0)
        [self loginWithUsername:self.txtUsername.text andPassword:self.txtPassword.text];
}

-(void)loginWithUsername:(NSString*)username andPassword:(NSString*)password{
    
    [DPHUser logInWithUsernameInBackground:username password:password
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            
                                            NSLog(@"login with success");
                                            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTab"];
                                            [self presentViewController:vc animated:YES completion:nil];
                                            
                                        } else {
                                            // The login failed. Check error to see why.
                                            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Alerta" message:error.description delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                            [alertView show];
                                        }
                                    }];
}

-(void)registerNewUserWithUsername:(NSString*)username andPassword:(NSString*)password{
    
    PFUser *user = [PFUser currentUser];
    user.username = username;
    user.password = password;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            
            NSLog(@"signup with success");
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTab"];
            [self presentViewController:vc animated:YES completion:nil];

        } else {
            NSString *errorString = [error userInfo][@"error"];
            // Show the errorString somewhere and let the user try again.
            
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:@"Alerta" message:errorString delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
        }
    }];
}

#pragma mark TEXTFIELD

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
