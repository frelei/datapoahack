//
//  DPHUser.h
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHBaseModel.h"
#import <Parse/Parse.h>

typedef enum
{
    NONE = -1,
    OP = 0,
    ON,
    BP,
    BN,
    AP,
    AN,
    ABP,
    ABN
    
} bloodType;

@interface DPHUser : PFUser<PFSubclassing>

+ (NSString *)parseClassName;

@property(nonatomic)NSNumber* bloodType;
@property(nonatomic)NSString* height;
@property(nonatomic)NSString* name;
@property(nonatomic)NSString* phone;
@property(nonatomic)NSString* weight;
@property(nonatomic)NSString* widgetMessage;
@property(nonatomic)NSDate* dateOfBirth;
@property(nonatomic)NSData* image;

-(NSString *)getBloodTypeString;
-(NSString*)getDateOfBirthFormatted;
//-(UIImage*)getUIImage;
-(BOOL)hasToSetProfile;
-(NSString*)getName;


@end
