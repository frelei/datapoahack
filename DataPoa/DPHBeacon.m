//
//  FBTBeacon.m
//  Beat
//
//  Created by Rodrigo Leite on 11/10/14.
//  Copyright (c) 2014 Flyhigh. All rights reserved.
//

#import "DPHBeacon.h"
#import "DPHSetting.h"

@interface DPHBeacon () <CLLocationManagerDelegate>

@property (nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL doingCheckin;

@end


@implementation DPHBeacon

#pragma mark - Inicitialize


+ (id) initialize
{
    static DPHBeacon *beacon = nil;
    static dispatch_once_t onceToken=0;
    dispatch_once(&onceToken, ^{
        beacon = [[self allocWithZone:nil] init];
    });
    return beacon;
}


- (id)init
{
    self = [super init];
    if (self) {
        _doingCheckin = NO;
        NSLog(@"Starting beacon....");
        // Initialize location manager and set ourselves as the delegate
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager requestWhenInUseAuthorization];
        
        
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"69BFE5D9-0E83-4B51-ADA1-093856BEE8C2"];
        self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                 identifier:@"br.com.flyhigh.Beat-Beacon"];
        
        // Tell location manager to start monitoring for the beacon region
        [self.locationManager startMonitoringForRegion:self.beaconRegion];
        [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    }
    return self;
}

#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    NSLog(@"Enter region!!!");
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    NSLog(@"Exit region!!!");
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"Region: %@, Error: %@", region,[error description]);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"beacon error: %@", [error description]);
}


//When a region is entered, we tell locationManager to start looking for beacons in the region.
-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    // Beacon found!
    if(beacons.count > 0) {
        CLBeacon *nearestBeacon = [beacons firstObject];
        
        // retrieve the beacon data from its properties
       __unused   NSString *uuid = nearestBeacon.proximityUUID.UUIDString;
       __unused   NSString *major = [NSString stringWithFormat:@"%@", nearestBeacon.major];
       __unused  NSString *minor = [NSString stringWithFormat:@"%@", nearestBeacon.minor];

        // Do the checkIn
        
        switch (nearestBeacon.proximity) {
            case CLProximityImmediate:
            {
                NSLog(@" immediate proximity of the beacon: %@", minor);

                if (_doingCheckin == NO)
                {
                    _doingCheckin = YES;
                    // mandar push aqui
                    
                }
                
             }
                break;
            case CLProximityNear:
                NSLog(@"near the beacon: %@", minor);

                break;
                
            case CLProximityFar:
                NSLog( @" far away from the beacon: %@", minor );

                break;
                
             case CLProximityUnknown:
                NSLog(@"Distance of beacon unkown: %@", minor);
                
                break;
        }
        
        
   //     NSLog(@"uuid: %@ major: %@ minor: %@", uuid, major, minor);

    }
}


@end


