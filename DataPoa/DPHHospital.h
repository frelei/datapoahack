//
//  DPHHospital.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DPHHospital : NSObject<NSCoding>
@property (nonatomic) int ID;
@property (nonatomic) NSString *instituteType;
@property (nonatomic) NSString *emergencyType;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *phone;
@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) NSString *linkPT;
@property (nonatomic) NSString *linkEN;
@property (nonatomic) NSString *linkES;
@property (nonatomic) NSString *specialityPT;
@property (nonatomic) NSString *specialityEN;
@property (nonatomic) NSString *specialityES;

@end
