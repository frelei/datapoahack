//
//  DPHPushNotification.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHPushNotification.h"
#import <Parse/Parse.h>
#import "DPHSetting.h"

@implementation DPHPushNotification

+ (void)sendPushNotificationToUsers:(NSArray *)list
{
 
    [[DPHSetting shareInstance] testBackgroundRefresh];
    // Build a query to match users with a birthday today
    PFQuery *innerQuery = [PFUser query];
    
    // Use hasPrefix: to only match against the month/date
    [innerQuery whereKey:@"objectId" containedIn:list];
    
    // Build the actual push notification target query
    PFQuery *query = [PFInstallation query];
    
    // only return Installations that belong to a User that
    // matches the innerQuery
    [query whereKey:@"user" matchesQuery:innerQuery];
    
    // Send the notification.
    PFPush *push = [[PFPush alloc] init];
    [push setQuery:query];
    [push setMessage:@"Happy Birthday!"];
    [push sendPushInBackground];
}

#warning VERIFY PUSH TO USERS LIST
+ (void)verifyHospitalToSendPush:(DPHHospital *)hospital
{
    NSArray *list = [[DPHSetting shareInstance] getLastHospitalWithDate];
    if (list.count >= 2)
    {
        DPHHospital *hospital2 = [NSKeyedUnarchiver unarchiveObjectWithData:list[0]];
        
        if (hospital2.ID == hospital.ID)
        {
            NSDate *date = list[1];
            
            if ([DPHPushNotification getTimeDifferenceFromDate:date] <= 0)
            {
                [DPHPushNotification sendPushNotificationToUsers:[[NSArray alloc] init]];
            }
        }
        else
        {
            [[DPHSetting shareInstance] storeLastHospital:hospital];
        }
    }
    else
    {
        [[DPHSetting shareInstance] storeLastHospital:hospital];
    }
}

+ (NSTimeInterval)getTimeDifferenceFromDate:(NSDate *)date
{
    if (date)
    {
        NSDateComponents *dateComponents = [NSDateComponents new];
        
        dateComponents.minute = 1;
        
        NSDate *newDate = [[NSCalendar currentCalendar]dateByAddingComponents:dateComponents
                                                                       toDate:date
                                                                      options:0];
        return [newDate timeIntervalSinceDate:[[NSDate alloc] init]];
    }
    return 0;

}
@end
