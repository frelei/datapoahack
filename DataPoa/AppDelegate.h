//
//  AppDelegate.h
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

