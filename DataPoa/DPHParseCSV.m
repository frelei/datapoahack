//
//  DPHParseCSV.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHParseCSV.h"
#import "DPHHospital.h"

@implementation DPHParseCSV

+ (NSArray *)parsing:(NSString *)fileString;
{
    NSArray *arrayByLine = [fileString componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
    NSMutableArray *list = [[NSMutableArray alloc] init];
    int ID = 0;
    for (NSString *line in arrayByLine)
    {
        DPHHospital *hospital = [self processingLine:line withID:ID];
        if (hospital.name != nil)
            [list addObject:hospital];
        ID++;
    }
    return list;
}

+ (DPHHospital *)processingLine:(NSString*)line
                         withID:(int)ID
{
    NSArray *arrayData = [line componentsSeparatedByString:@";"];
    DPHHospital *dphHospital = [[DPHHospital alloc] init];
    if (arrayData.count >= 13 && ID > 0)
    {
        dphHospital.ID = ID;
        dphHospital.instituteType = arrayData[0];
        dphHospital.emergencyType = arrayData[1];
        dphHospital.name = arrayData[2];
        dphHospital.address = arrayData[3];
        dphHospital.phone = arrayData[4];
        dphHospital.latitude = [[arrayData[5] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        dphHospital.longitude = [[arrayData[6] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        dphHospital.linkPT = arrayData[7];
        dphHospital.linkEN = arrayData[8];
        dphHospital.linkES = arrayData[9];
        dphHospital.specialityPT = arrayData[10];
        dphHospital.specialityEN = arrayData[11];
        dphHospital.specialityES = arrayData[12];
    }
    return dphHospital;
}
@end
