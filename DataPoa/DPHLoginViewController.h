//
//  DPHLoginViewController.h
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>

@interface DPHLoginViewController : ViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

@end
