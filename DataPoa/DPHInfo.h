//
//  DPHInfo.h
//  DataPoa
//
//  Created by Fabio Barboza on 12/13/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DPHInfo : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *detail;

- (instancetype)initWithTitle:(NSString *)title
                    andDetail:(NSString *)detail;

@end
