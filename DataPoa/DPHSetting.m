//
//  DPHSetting.m
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHSetting.h"
#import <Parse/Parse.h>

@interface DPHSetting ()

@property(nonatomic) NSUserDefaults *defaults;
@property(nonatomic) NSUserDefaults *groupDefaults;

@end



@implementation DPHSetting


+(id) shareInstance{
    
    
    static DPHSetting *settings;
    static dispatch_once_t dipatch;
    dispatch_once(&dipatch, ^{
        settings = [[self alloc] init];
    });
    return settings;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _defaults = [NSUserDefaults standardUserDefaults];
        _groupDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.flyhigh.widget"];
    }
    return self;
}

- (void)storeHospitalList:(NSArray *)list
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:list];
    [_defaults setObject:data forKey:@"hospital"];
    [_groupDefaults setObject:data forKey:@"hospital"];
    [_defaults synchronize];
}

- (NSArray *)getHospitalList
{
    NSData *data = [_defaults objectForKey:@"hospital"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)storeLastHospital:(DPHHospital *)hospital
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:hospital];
    NSArray *list = [[NSArray alloc] initWithObjects:data, [[NSDate alloc] init], nil];
    [_defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:list] forKey:@"controlBackgroundRefresh"];
    [_defaults synchronize];
}

- (NSArray *)getLastHospitalWithDate
{
    return [NSKeyedUnarchiver unarchiveObjectWithData:[_defaults objectForKey:@"controlBackgroundRefresh"]];
}

- (void)testBackgroundRefresh
{
    [_defaults setObject:@"YES" forKey:@"testBackgroundRefresh"];
}

- (NSString *)getTestBackgroundRefresh
{
    return [_defaults objectForKey:@"testBackgroundRefresh"];
}

-(void)getFollowingUsersWithCallback:(void (^)(NSArray *))success
                             failure:(void (^) (NSError *)) failure{
    
    PFQuery* query = [PFQuery queryWithClassName:@"Following"];
    [query whereKey:@"followingUser" equalTo:[PFUser currentUser]];
    [query includeKey:@"followedUser"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu followings.", (unsigned long)objects.count);
            success(objects);
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            failure(error);
        }
    }];
}
#warning bruno, chama o metodo acima assim:

//DPHSetting* setting = [DPHSetting shareInstance];
//[setting getFollowingUsersWithCallback:^(NSArray * a) {
//    NSLog(@"%@",a);
//} failure:^(NSError *e) {
//    NSLog(@"%@",e);
//}];




@end
