//
//  DPHWSDataPoa.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DPHWSDataPoa : NSObject
- (void)getPlacesInformationWithCallback:(void (^)(NSArray *))callback;
@end
