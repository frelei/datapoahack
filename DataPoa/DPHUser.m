//
//  DPHUser.m
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHUser.h"
#import "DPHSetting.h"

@implementation DPHUser

@dynamic name;
@dynamic weight;
@dynamic height;
@dynamic widgetMessage;
@dynamic bloodType;
@dynamic phone;
@dynamic dateOfBirth;
@dynamic image;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"User";
}

-(NSString *)getBloodTypeString{
    NSArray* bloodTypes = @[@"O+",@"O-",@"B+",@"B-",@"A+",@"A-",@"AB+",@"AB-"];
    
    if (self.bloodType.integerValue == -1) {
        return nil;
    }
    else{
        return [bloodTypes objectAtIndex:self.bloodType.integerValue];
    }
}

-(NSString*)getDateOfBirthFormatted{
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateStyle:NSDateFormatterMediumStyle];
    
    NSString* response;
    
    if (self.dateOfBirth) {
        response = [df stringFromDate:self.dateOfBirth];
    }
    else{
        response = @"Sem data";
    }
    return response;
}

//-(UIImage *)getUIImage{
//    UIImage* uiimage;
//    
//    if (self.image) {
//        uiimage = [[UIImage alloc]initWithData:self.image];
//    }
//    else{
//        uiimage = [[UIImage alloc]init];
//    }
//    return uiimage;
//}

-(BOOL)hasToSetProfile{
    if (!self.name || !self.image) {
        return YES;
    }
    else{
        return NO;
    }
}

-(NSString*)getName{
    if (!self.name) {
        return @"no name";
    }
    else{
        return self.name;
    }
}

//@dynamic name;
//@dynamic weight;
//@dynamic height;
//@dynamic widgetMessage;
//@dynamic bloodType;
//@dynamic phone;
//@dynamic dateOfBirth;
//@dynamic image;
@end
