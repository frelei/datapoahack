//
//  ACCWebService.m
//  Atlanta Chamber
//
//  Created by Rodrigo Freitas Leite on 10/12/14.
//  Copyright (c) 2014 arctouch. All rights reserved.
//

#import "DPHWebService.h"
#import "AFNetworking.h"


@implementation DPHWebService
{
    NSString * finalUrl;
}

-(instancetype) initWithBaseUrl:(NSString *)urlBase
{
    self = [super init];
    
    if (self)
    {
        self.baseUrl = urlBase;
        finalUrl = [[NSString alloc] init];
    }
    
    return self;
}


- (void) constructFinalUrl
{
    finalUrl = _baseUrl;
    finalUrl = [finalUrl stringByAppendingString:_urlMethod];
}


#pragma mark - Execute GET

/**
 * Access a Get Request base in a url
 *
 * @param parameter It's a NSDictionary which is append in the end of url
 * @param callback  it's block that's calling when the answer is ready
 */

- (void)executeGETRequestSucessWithParamenter:(id)parameter
                                     callback:(void (^) (id response))callback
{
    [self constructFinalUrl];
    NSLog(@"HTTP GET Request -> %@", finalUrl);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:finalUrl
      parameters:parameter
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.urlGetParameter = nil;
              callback(responseObject);
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             self.urlGetParameter = nil;
             NSLog(@"Error Get Request");
             callback(0); // Just passing some value to get error later

         }];
}


#pragma mark - Execute POST

/**
 * Access a POST Request base in a url
 *
 * @param parameter It's a NSDictionary which is append in the end of url
 * @param callback  it's block that's calling when the answer is ready
 */

-(void)executePOSTRequestWithParamenter:(id)parameter
                                 callback:(void (^)(id) )callback
{
    [self constructFinalUrl];
    NSLog(@"HTTP POST Request -> %@", finalUrl);

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:finalUrl parameters:parameter
          success:^(AFHTTPRequestOperation *operation, id responseObject){
              
            self.urlGetParameter = nil;
            callback(responseObject);

          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              self.urlGetParameter = nil;
              NSLog(@"Error Post Request: %@", [error description]);
              callback(0); // Just passing some value get error later
            
          }];
}

@end
