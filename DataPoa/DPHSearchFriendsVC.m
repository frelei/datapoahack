//
//  DPHSearchFriendsVC.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHSearchFriendsVC.h"
#import <Parse/Parse.h>
#import "DPHUser.h"
#import "DPHFollowing.h"


@interface DPHSearchFriendsVC ()
@property(nonatomic)NSMutableArray* usersToBeFollowed;
@property(nonatomic)NSMutableArray* displayArray;
@end

@implementation DPHSearchFriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.displayArray = [NSMutableArray new];
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    [self.searchBar setDelegate:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#warning chamar esse método quando for resign modal
-(void)saveInParseServerTheLinkedUsers{
    
    for (int i=0; i<self.usersToBeFollowed.count; i++) {
        
        DPHFollowing* following = [DPHFollowing objectWithClassName:[DPHFollowing parseClassName]];
        following.followedUser=self.usersToBeFollowed[i];
        following.followingUser=(DPHUser*)[PFUser currentUser];
        [following save:nil];
        
        if (i==self.usersToBeFollowed.count-1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (IBAction)confirmButton:(UIBarButtonItem *)sender {
    [self saveInParseServerTheLinkedUsers];
}

-(void)getUsersWithUsername:(NSString*)username{
    
    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"username" equalTo:username];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
            self.displayArray=(NSMutableArray*)objects;
            [self.tableView reloadData];
            
            for (PFObject *object in objects) {
                NSLog(@"%@", object.objectId);
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.usersToBeFollowed addObject:[self.displayArray objectAtIndex:indexPath.row]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.displayArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel* lblUsername = (UILabel*)[cell viewWithTag:2];
    lblUsername.text=[(DPHUser*)self.displayArray[indexPath.row] username];
    
    return cell;
}

#warning chamar este método cada vez que o user selecionar um user como contato de emergência
-(void)didSelectUser:(DPHUser*)user{
    [self.usersToBeFollowed addObject:user];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self getUsersWithUsername:searchBar];
}

-(NSMutableArray *)usersToBeFollowed{
    if (!_usersToBeFollowed) {
        _usersToBeFollowed = [[NSMutableArray alloc]init];
    }
    return _usersToBeFollowed;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
