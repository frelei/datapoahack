//
//  DPHSearchFriendsVC.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPHSearchFriendsVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
