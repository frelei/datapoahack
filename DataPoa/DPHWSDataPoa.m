//
//  DPHWSDataPoa.m
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHWSDataPoa.h"
#import "DPHWebService.h"
#import "DPHParseCSV.h"

static NSString *urlBase;
@implementation DPHWSDataPoa
{
    DPHWebService *webService;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        urlBase = @"http://datapoa.com.br/storage/";
        webService = [[DPHWebService alloc] initWithBaseUrl:urlBase];
    }
    return self;
}

- (void)getPlacesInformationWithCallback:(void (^)(NSArray *))callback
{
    webService.urlMethod = @"f/2014-03-24T17%3A24%3A39.984Z/doctorpoa.csv";
    
    [webService executeGETRequestSucessWithParamenter:nil callback:^(id response) {
//        NSLog(@"%@", response);
        NSString *fileString = [[NSString alloc] initWithData:response encoding:NSASCIIStringEncoding];
        callback([DPHParseCSV parsing:fileString]);
    }];
}
@end
