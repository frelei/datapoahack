//
//  DPHFollowing.h
//  DataPoa
//
//  Created by Felipe Polidori Rios on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "DPHUser.h"

@interface DPHFollowing : PFObject<PFSubclassing>
@property(nonatomic)DPHUser* followingUser;
@property(nonatomic)DPHUser* followedUser;

@end
