//
//  ACCBaseModel.m
//  Atlanta Chamber
//
//  Created by Rodrigo Freitas Leite on 10/12/14.
//  Copyright (c) 2014 arctouch. All rights reserved.
//

#import "DPHBaseModel.h"

@implementation DPHBaseModel
@synthesize attrs;

- (id)initWithAttrs:(NSMutableDictionary *)attributes
{
    self = [super init];
    if (self)
    {
        if ([attributes isKindOfClass:[NSDictionary class]])
            attrs = [[NSMutableDictionary alloc] initWithDictionary:attributes];
        else
            attrs = attributes;
    }
    
    return self;
}

- (NSString *) description
{
    return attrs.description;
}

- (NSInteger) ID
{
    return [self returnNSInteger:[attrs objectForKey:@"id"]];
}

- (NSString *)returnNSString:(id)object
{
    if (object != [NSNull null])
        return object;
    else
    {
        NSString *a;
        return a;
    }
}

- (NSInteger)returnNSInteger:(id)object
{
    if (object != [NSNull null])
        return [object integerValue];
    else
    {
        NSInteger a = 0;
        return a;
    }
}

- (CGFloat)returnCGFloat:(id)object
{
    if (object != [NSNull null])
        return [object floatValue];
    else
    {
        CGFloat a = 0.0;
        return a;
    }
}

- (long)returnLong:(id)object
{
    if (object != [NSNull null])
        return [object longValue];
    else
    {
        long a = 0;
        return a;
    }
}

- (BOOL)returnBOOL:(id)object
{
    if (object != [NSNull null])
        return [object boolValue];
    else
    {
        return NO;
    }
}

- (NSDate *)returnNSDate:(id)object withFormat:(NSString *)format
{
    if (object != [NSNull null])
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]]];
        //        [dateFormat setTimeZone:[NSTimeZone localTimeZone]];
        [dateFormat setDateFormat:format];
        return [dateFormat dateFromString:object];
    }
    else
    {
        NSDate *a;
        return a;
    }
}

- (NSDate *)returnNSDate:(id)object
{
    return [self returnNSDate:object withFormat:@"yyyy-MM-dd HH:mm:ss"];
}

- (NSDate *)returnNSDateFromDate:(id)object
{
    if (object != [NSNull null])
    {
        return object;
    }
    else
    {
        NSDate *a;
        return a;
    }
}


- (NSDate *) returnNSDateFromTimeInterval:(id)object
{
    if (object != [NSNull null])
    {
        NSDate *a = [NSDate dateWithTimeIntervalSince1970:[object longLongValue]];
        return a;
    }
    else
    {
        NSDate *a;
        return a;
    }
}

- (NSData *) returnNSData:(id)object
{
    if (object != [NSNull null])
    {
        NSData *a = object;
        return a;
    }
    else
    {
        NSData *a;
        return a;
    }
}

- (NSDictionary *)returnNSDictionary:(id)object
{
    if (object != [NSNull null])
        return (NSDictionary *)object;
    else
    {
        NSDictionary *a;
        return a;
    }
}

- (NSArray *)returnNSArray:(id)object
{
    if (object != [NSNull null])
        return (NSArray *)object;
    else
    {
        NSArray *a;
        return a;
    }
}

- (NSArray *) returnNSArray:(id)object withObjectType:(Class) type
{
    if (object != [NSNull null])
    {
        NSMutableArray *arrayAux = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dict in object)
        {
            [arrayAux addObject:[self returnObject:dict withType:type]];
        }
        
        return arrayAux;
    }
    else
    {
        NSArray *a;
        return a;
    }
}

- (DPHBaseModel *) returnObject:(id)object withType:(Class) type {
    if (object != [NSNull null])
    {
        DPHBaseModel *a = [[type alloc] initWithAttrs:object];
        return a;
    }
    else
    {
        DPHBaseModel *a;
        return a;
    }
}


- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:attrs forKey:@"attrs"];
}

- (id) initWithCoder: (NSCoder *)coder
{
    if (self = [super init])
    {
        attrs = [coder decodeObjectForKey:@"attrs"];
    }
    
    return self; // this is missing in the example above
    
}

@end
