//
//  AppDelegate.m
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "AppDelegate.h"
#import "DPHSetting.h"
#import "DPHHospital.h"
#import "DPHLocationManager.h"
#import "DPHBeacon.h"
#import <Parse/Parse.h>
#import "DPHPushNotification.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
{
    CLLocationManager *locationManager;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Parse setApplicationId:@"NmXZKiCmzBllCuUAaQzbWAVRCIXWgSx0cdJxRw7h"
                  clientKey:@"h19aEYL6aP919iuZXMfnMc8QhAooPeGEasi9F9AN"];
    
    [DPHSetting shareInstance];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestAlwaysAuthorization];
    [DPHBeacon initialize];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    return YES;
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - LOCAL NOTIFICATION


-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    // Minimum time for the fetch - 30secounds
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    return true;
}



-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location;
    if (locations.count > 0)
    {
        [locationManager stopUpdatingLocation];
        location = locations[0];
        DPHHospital *hospital = [DPHLocationManager verifyLocationNearHospital:location];
        if (hospital)
        {
            [DPHPushNotification verifyHospitalToSendPush:hospital];
        }
    }
    NSLog(@"%@", location);
}

@end





