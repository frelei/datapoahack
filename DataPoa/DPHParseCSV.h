//
//  DPHParseCSV.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DPHParseCSV : NSObject
+ (NSArray *)parsing:(NSString *)fileString;
@end
