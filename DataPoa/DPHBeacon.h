//
//  FBTBeacon.h
//  Beat
//
//  Created by Rodrigo Leite on 11/10/14.
//  Copyright (c) 2014 Flyhigh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

/////////////////////////////////
// This class need to be a singleton
////////////////////////////////

@interface DPHBeacon : NSObject 

+(id) initialize;

@end
