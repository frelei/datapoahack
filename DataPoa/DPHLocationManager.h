//
//  DPHLocationManager.h
//  DataPoa
//
//  Created by Bruno Vieira Bulso on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h> 
#import "DPHHospital.h"

@interface DPHLocationManager : NSObject
+ (DPHHospital *)verifyLocationNearHospital:(CLLocation *)location;
@end
