//
//  ACCWebService.h
//  Atlanta Chamber
//
//  Created by Rodrigo Freitas Leite on 10/12/14.
//  Copyright (c) 2014 arctouch. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *
 * When Istantiate this class always need pass the base url and the method
 * that you want get the data before call executeGETRequest or executePOSTRequest
 *
 */


@interface DPHWebService : NSObject

@property (nonatomic) NSString *baseUrl;
@property (nonatomic) NSString *urlMethod;
@property (nonatomic) NSString *urlGetParameter;


- (instancetype) initWithBaseUrl : (NSString *) urlBase;

- (void)executeGETRequestSucessWithParamenter:(id)parameter
                                     callback:(void (^) (id response))callback;

- (void)executePOSTRequestWithParamenter:(id)parameter
                                 callback:(void (^)(id) )callback;

@end
