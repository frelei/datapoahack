//
//  DPHProfileUpdateVC.m
//  DataPoa
//
//  Created by Fabio Barboza on 12/13/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHProfileUpdateVC.h"

@interface DPHProfileUpdateVC ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DPHProfileUpdateVC
{
    UITextView *txtUserInformation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self configureTableViewSettings];
}

#pragma mark TABLEVIEW

- (void)configureTableViewSettings
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tabBarController.tabBar.frame.size.height)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"PhotoCell" forIndexPath:indexPath];
            
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
//            imageView.image = [self.user getUIImage];
            imageView.layer.borderWidth = 1.;
            imageView.layer.borderColor = [UIColor clearColor].CGColor;
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = YES;
            
            break;
        }
        case 1:
        {
            //Description
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"InformationCell" forIndexPath:indexPath];
            
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
            lblTitle.text = @"Informações Úteis";
            UITextView *txtInformation = (UITextView *)[cell viewWithTag:2];
            txtInformation.text = @"A+";
            txtUserInformation = txtInformation;
            
            break;
        }
        case 2:
        {
            //tipo sanguineo
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
            lblTitle.text = @"Tipo Sanguineo";
            UILabel *lblInformation = (UILabel *)[cell viewWithTag:2];
            lblInformation.text = @"A+";
            
            break;
        }
        case 3:
        {
            //aniversario
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
            lblTitle.text = @"Birthday";
            UILabel *lblInformation = (UILabel *)[cell viewWithTag:2];
            lblInformation.text = @"13 de junho de 1990";
            
            break;
        }
        case 4:
        {
            //altura
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
            lblTitle.text = @"Altura";
            UILabel *lblInformation = (UILabel *)[cell viewWithTag:2];
            lblInformation.text = @"190cm";
            
            break;
        }
        case 5:
        {
            //peso
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
            lblTitle.text = @"Peso";
            UILabel *lblInformation = (UILabel *)[cell viewWithTag:2];
            lblInformation.text = @"90kg";
            
            break;
        }
            
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 87;
    if (indexPath.row == 1)
        return 123;
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

#pragma mark TRIGGERS

- (IBAction)confirmAction:(UIBarButtonItem *)sender {
#warning RIOS LINDO SALVE AQUI :D
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtUserInformation resignFirstResponder];
}

@end
