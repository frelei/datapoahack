//
//  DPHSetting.h
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 13/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPHHospital.h"

@interface DPHSetting : NSObject

+(id) shareInstance;
- (void)storeHospitalList:(NSArray *)list;
- (NSArray *)getHospitalList;
- (NSArray *)getLastHospitalWithDate;
- (void)storeLastHospital:(DPHHospital *)hospital;
- (NSString *)getTestBackgroundRefresh;
- (void)testBackgroundRefresh;
-(void)getFollowingUsersWithCallback:(void (^)(NSArray *))success
                             failure:(void (^) (NSError *)) failure;
@end
