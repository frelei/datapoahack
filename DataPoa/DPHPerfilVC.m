//
//  DPHPerfilViewController.m
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import "DPHPerfilVC.h"
#import "DPHUser+DPHUserCategory.h"

@interface DPHPerfilVC ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DPHPerfilVC
{
    NSMutableArray *userInformation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = (DPHUser*)[DPHUser currentUser];
    [self configureTableViewSettings];
}

#pragma mark DATA

- (void)getNotifications
{
    self.tableViewList = [[NSMutableArray alloc] init];
#warning PUXAR AQUI
}

- (void)getUserInformation
{
    userInformation = [[NSMutableArray alloc] init];
    if (self.user.bloodType >= 0)
        [userInformation addObject:[[DPHInfo alloc] initWithTitle:@"Tipo Sanguineo" andDetail:[self.user getBloodTypeString]]];
    if (self.user.height)
        [userInformation addObject:[[DPHInfo alloc] initWithTitle:@"Altura" andDetail:self.user.height]];
    if (self.user.weight)
        [userInformation addObject:[[DPHInfo alloc] initWithTitle:@"Peso" andDetail:self.user.weight]];
    if (self.user.phone)
        [userInformation addObject:[[DPHInfo alloc] initWithTitle:@"Telefone" andDetail:self.user.phone]];
}

#pragma mark TABLEVIEW

- (void)configureTableViewSettings
{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tabBarController.tabBar.frame.size.height)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    if (indexPath.row == 0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        imageView.image = [self.user getUIImage];
        imageView.layer.borderWidth = 1.;
        imageView.layer.borderColor = [UIColor clearColor].CGColor;
        imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        imageView.clipsToBounds = YES;
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:2];
        lblName.text = self.user.name ? self.user.name : @"sem nome";
        UILabel *lblBirthday = (UILabel *)[cell viewWithTag:3];
        lblBirthday.text=[self.user getDateOfBirthFormatted];
    }
    else if (indexPath.row == 1 && self.user.widgetMessage.length > 0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"UserInformationsCell" forIndexPath:indexPath];
        
        UILabel *lblWidgetText = (UILabel *)[cell viewWithTag:2];
        [lblWidgetText removeFromSuperview];
        CGRect rect = lblWidgetText.frame;
        
        UILabel *label = [[UILabel alloc] initWithFrame:rect];
        label.tag = 2;
        label.font = lblWidgetText.font;
        label.text = self.user.widgetMessage;
        label.numberOfLines = 0;
        [label sizeToFit];
        [cell addSubview:label];
    }
    else
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"InfoCell" forIndexPath:indexPath];
        
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:1];
        UILabel *lblDetail = (UILabel *)[cell viewWithTag:2];
        DPHInfo *info;
        
        if (self.user.widgetMessage.length > 0)
            info = userInformation[indexPath.row - 2];
        else
            info = userInformation[indexPath.row - 1];
        
        lblTitle.text = info.title;
        lblDetail.text = info.detail;

    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 91;
    else if (indexPath.row == 1 && self.user.widgetMessage.length > 0)
    {
        
        UILabel *lblComment = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width -18, 21)];
        lblComment.text = self.user.widgetMessage;
        lblComment.numberOfLines = 0;
        [lblComment sizeToFit];
        
        return 62 + lblComment.frame.size.height;
    }
    return 58;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + userInformation.count;
}

#pragma mark TRIGGERS

- (IBAction)editUser:(UIButton *)sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileEditingVC"];
    [self presentViewController:vc animated:YES completion:nil];    
}

@end
