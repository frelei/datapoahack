//
//  DPHPerfilViewController.h
//  DataPoa
//
//  Created by Rodrigo Freitas Leite on 12/12/14.
//  Copyright (c) 2014 FlyHigh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DPHUser.h"
#import "DPHInfo.h"

@interface DPHPerfilVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSMutableArray *tableViewList;
@property (nonatomic) DPHUser *user;

@end
